// This file is released under the 3-clause BSD license. See COPYING-BSD.

function cleaner_src()
    langage_src = ["c"];
    path_src = get_absolute_file_path("cleaner_src.sce");
    exec(fullfile(path_src, langage_src, "cleaner.sce"));
endfunction

cleaner_src();
clear cleaner_src; 
