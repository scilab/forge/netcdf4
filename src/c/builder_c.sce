function builder_c()

    src_c_path = get_absolute_file_path("builder_c.sce");
    thirdparty_path = fullfile(src_c_path, "../../thirdparty");

    lib_name = "scinetcdf_hl";
    file_names = ["nccreate.c", "ncread.c", "ncwrite.c", "ncwriteatt.c", "ncreadatt.c", "scinetcdf_hl_utils.c"];
    entry_points = ["imp_nccreate", "imp_ncread", "imp_ncwrite", "imp_ncwriteatt", "imp_ncreadatt"];

    CFLAGS = "";
    CC = "";
    LIBS = "";
    LDFLAGS = "";
    FFLAGS = "";

    os = getos();
    [version, opts] = getversion();
    arch = opts(2);
    thirdparty_path = fullpath(fullfile(thirdparty_path, os, arch));
    if os == "Windows" then
        CFLAGS = "-DDLL_NETCDF -DDLL_SCINETCDF_HL_EXPORTS";
        if findmsvccompiler() <> "unknown" then
            LDFLAGS = fullpath(thirdparty_path + "/lib/netcdf.lib");
        else
            LDFLAGS = msprintf("-L""%s"" -lnetcdf", fullfile(thirdparty_path, "lib"));
        end
    else
        CFLAGS = "-g";
        LDFLAGS = "-L" + fullfile(thirdparty_path, "lib") + " -lnetcdf";
    end

    CFLAGS = CFLAGS + " " + ilib_include_flag(fullfile(thirdparty_path, "include"));
    CFLAGS = CFLAGS + " " + ilib_include_flag(src_c_path);

    tbx_build_src(entry_points, ..
      file_names, ..
      "c", ..
      src_c_path, ..
      LIBS, ..
      LDFLAGS, ..
      CFLAGS, ..
      FFLAGS, ..
      CC, ..
      lib_name);
endfunction

builder_c();
clear builder_c;
