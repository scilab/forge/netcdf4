#ifndef _SCINETCDF_HL_TYPES
#define _SCINETCDF_HL_TYPES

#include "netcdf.h"

typedef enum option_type
{
    OT_SINGLE = 0,
    OT_DOUBLE = 1,
    OT_STRING = 10,
    OT_LIST   = 17,
    OT_CHAR = 100,
    OT_INT8 = 101,
    OT_INT16 = 102,
    OT_INT32 = 104,
    OT_INT64 = 108,
    OT_UINT8 = 111,
    OT_UINT16 = 112,
    OT_UINT32 = 114,
    OT_UINT64 = 118
} option_type;

typedef struct st_option
{
    char *name;
    option_type type;
    void *value;
} st_option;

typedef struct st_option_list
{
    int nboptions;
    st_option *options;
} st_option_list;

typedef struct st_data
{
    nc_type type;
    int nbdims;
    size_t *dims;
    void *data;
} st_data;

typedef struct st_attdata
{
    nc_type type;
    size_t len;
    void *data;
} st_attdata;

#endif
