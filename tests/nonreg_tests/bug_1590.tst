// Test for issue 1590
// http://forge.scilab.org/index.php/p/netcdf4/issues/1590/

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = getncfilename();
nccreate(filepath, "var", "Dimensions", list("row", 10, "dim1", 1));
expected_vardata = ones(10, 1);
ncwrite(filepath, "var",  expected_vardata);
vardata = ncread(filepath, "var");
assert_checkequal(vardata, expected_vardata);
