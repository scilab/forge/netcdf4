// Test for issue 1593
// http://forge.scilab.org/index.php/p/netcdf4/issues/1593/

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = getncfilename();
nccreate(filepath, "var", "Dimensions", list("rows", 3, "cols", 4, "mats", 2)) ;
expected_vardata = ones(3, 4, 2);
ncwrite(filepath, "var", expected_vardata) ;
vardata = ncread(filepath, "var");
assert_checkequal(vardata, expected_vardata);
