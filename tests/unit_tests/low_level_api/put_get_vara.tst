scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function filepath = test_put_get_vara_type(typeName, typeId, values)
  filepath = fullfile(TMPDIR, msprintf("put_get_vara_%s.nc", typeName));
  [ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
  assert_checkreturn(ret, 0, "nc_create(...)");

  dims = size(values);
  nbDims = max(size(find(dims > 1), 'c'), 1);

  dimids = [];
  for i=1:nbDims
    [ret, dimid] = nc_def_dim(ncid, msprintf("dim%d", i), dims(i));
    assert_checkreturn(ret, 0, "nc_def_dim(...)");
    dimids = [dimids, dimid];
  end
  assert_checkreturn(ret, 0, "nc_def_dim(...)");

  [ret, varid] = nc_def_var(ncid, "var", typeId, dimids);
  assert_checkreturn(ret, 0, "nc_def_var(...)");

  nbValues = prod(dims);
  subs = ind2sub(dims, 1:nbValues);

  subCount = [1, 1];

  for i = 1:nbValues
    subStart = subs(i, :);
    subEnd = subStart + subCount - 1;
    subValues = values(subStart(1):subEnd(1), subStart(2):subEnd(2));
    arrayIn = mat2array(subValues, typeName);
    cmd = msprintf("ret = nc_put_vara_%s(ncid, varid, subStart-1, subCount, arrayIn)", lowlevel_function_suffix(typeName));
    execstr(cmd);
    assert_checkreturn(ret, 0, cmd);
    deleteArray(arrayIn, typeName, nbValues);
  end

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");

  [ret, ncid] = nc_open(filepath, NC_NOWRITE);
  assert_checkreturn(ret, 0, "nc_open(...)");

  [ret, varid] = nc_inq_varid(ncid, "var");
  assert_checkreturn(ret, 0, "nc_inq_varid(...)");

  subCountTot = prod(subCount);
  arrayOut = newArray(typeName, subCountTot);

  for i = 1:nbValues
    subStart = subs(i, :);
    cmd = msprintf("ret = nc_get_vara_%s(ncid, varid, subStart-1, subCount, arrayOut)", lowlevel_function_suffix(typeName));
    execstr(cmd);
    assert_checkreturn(ret, 0, cmd);
    subEnd = subStart + subCount - 1;
    expectedSubValues = values(subStart(1):subEnd(1), subStart(2):subEnd(2));
    subValuesOut = array2mat(arrayOut, subCountTot, typeName);
    assert_checkequal(subValuesOut, expectedSubValues);
  end
  deleteArray(arrayOut, typeName, nbValues);

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");
endfunction

value = [1 2; 3 4];
test_put_get_vara_type("schar", NC_BYTE, value);
test_put_get_vara_type("uchar", NC_UBYTE, value);
test_put_get_vara_type("short", NC_SHORT, value);
test_put_get_vara_type("ushort", NC_USHORT, value);
test_put_get_vara_type("int", NC_INT, value);
test_put_get_vara_type("uint", NC_UINT, value);
test_put_get_vara_type("uchar", NC_FLOAT, value);
test_put_get_vara_type("double", NC_DOUBLE, value);

test_put_get_vara_type("char", NC_CHAR, ["a" "b"; "c" "d"]);
//test_put_get_vara_type("string", NC_STRING, ["aa" "bb"; "cc" "dd"]);

