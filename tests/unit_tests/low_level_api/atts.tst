scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

OLD_NAME = "Constantinople";
NEW_NAME = "Istanbul";
CONTENTS = "Lots of people!";
lenContents = length(CONTENTS);

filepath = fullfile(TMPDIR, "tst_atts.nc");

[ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
assert_checkreturn(ret, 0, "nc_create(...)");

charArray = new_CharArray(lenContents);
for i=1:lenContents
   CharArray_setitem(charArray, i-1, part(CONTENTS, i:i));
end

ret = nc_put_att_text(ncid, NC_GLOBAL, OLD_NAME, lenContents, charArray);
assert_checkreturn(ret, 0, "nc_put_att_text(...)");
delete_CharArray(charArray);

ret = nc_rename_att(ncid, NC_GLOBAL, OLD_NAME, NEW_NAME);
assert_checkreturn(ret, 0, "nc_rename_att(...)");

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...) (first)");

[ret, ncid] = nc_open(filepath, NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

[ret, attid] = nc_inq_attid(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_inq_attid(...)");
assert_checkequal(attid, 0);

[ret, attlen] = nc_inq_attlen(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_inq_attlen(...)");
assert_checkequal(attlen, lenContents);

charArray = new_CharArray(lenContents);
ret = nc_get_att_text(ncid, NC_GLOBAL, NEW_NAME, charArray);
assert_checkreturn(ret, 0, "nc_get_att_text(...)");
att_value = [];
for i=1:attlen
   att_value = att_value + CharArray_getitem(charArray, i-1);
end
delete_CharArray(charArray);
assert_checkequal(att_value, CONTENTS);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...) (second)");

deletefile(filepath);
