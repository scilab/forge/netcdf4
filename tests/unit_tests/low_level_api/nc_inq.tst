scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = fullfile(scinetcdf_path, "tests/unit_tests/data/nc_inq.nc");

[ret, ncid] = nc_open(filepath, NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

[ret, nbdims, nbvars, nbatts, unlimdimids] = nc_inq(ncid);
assert_checkreturn(ret, 0, "nc_inq(...)");
assert_checkequal(nbdims, 4);
assert_checkequal(nbvars, 3);
assert_checkequal(nbatts, 2);
assert_checkequal(unlimdimids, 3);

[ret, nbdims] = nc_inq_ndims(ncid);
assert_checkreturn(ret, 0, "nc_inq_ndims(...)");
assert_checkequal(nbdims, 4);

[ret, nbvars] = nc_inq_nvars(ncid);
assert_checkreturn(ret, 0, "nc_inq_nvars(...)");
assert_checkequal(nbvars, 3);

[ret, nbatts] = nc_inq_natts(ncid);
assert_checkreturn(ret, 0, "nc_inq_natts(...)");
assert_checkequal(nbatts, 2);

[ret, unlimdimids] = nc_inq_unlimdim(ncid);
assert_checkreturn(ret, 0, "nc_inq_unlimdim(...)");
assert_checkequal(unlimdimids, 3);

[ret, dimids] = nc_inq_dimids(ncid, 0);
assert_checkreturn(ret, 0, "nc_inq_dimidS(...)");
assert_checkequal(dimids, 0:3);

[ret, ncformat] = nc_inq_format(ncid);
assert_checkreturn(ret, 0, "nc_inq_format(...)");
assert_checkequal(int32(ncformat), NC_FORMAT_NETCDF4);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");



