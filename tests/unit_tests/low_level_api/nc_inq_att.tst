scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = fullfile(scinetcdf_path, "tests/unit_tests/data/nc_inq.nc");

[ret, ncid] = nc_open(filepath, NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

[ret, att1type, att1len] = nc_inq_att(ncid, NC_GLOBAL, "att1");
assert_checkreturn(ret, 0, "nc_inq_att(...)");
assert_checkequal(int32(att1type), NC_DOUBLE);
assert_checkequal(att1len, 1);

[ret, att1type] = nc_inq_atttype(ncid, NC_GLOBAL, "att1");
assert_checkreturn(ret, 0, "nc_inq_atttype(...)");
assert_checkequal(int32(att1type), NC_DOUBLE);

[ret, att1len] = nc_inq_attlen(ncid, NC_GLOBAL, "att1");
assert_checkreturn(ret, 0, "nc_inq_attlen(...)");
assert_checkequal(att1len, 1);

[ret, att1name] = nc_inq_attname(ncid, NC_GLOBAL, 0);
assert_checkreturn(ret, 0, "nc_inq_attname(...)");
assert_checkequal(att1name, "att1");

[ret, att1id] = nc_inq_attid(ncid, NC_GLOBAL, "att1");
assert_checkreturn(ret, 0, "nc_inq_attid(...)");
assert_checkequal(att1id, 0);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");

