scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function filepath = test_put_get_var_type(typeName, typeId, values)
  filepath = fullfile(TMPDIR, msprintf("put_get_var_%s.nc", typeName));
  [ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
  assert_checkreturn(ret, 0, "nc_create(...)");

  if typeName <> "char" then
    dims = size(values);
    nbDims = max(size(find(dims > 1), 'c'), 1);
  else
    dims = length(values);
    nbDims = 1;
  end

  nbValues = prod(dims);

  dimids = [];
  for i=1:nbDims
    [ret, dimid] = nc_def_dim(ncid, msprintf("dim%d", i), dims(i));
    assert_checkreturn(ret, 0, "nc_def_dim(...)");
    dimids = [dimids, dimid];
  end
  assert_checkreturn(ret, 0, "nc_def_dim(...)");

  [ret, varid] = nc_def_var(ncid, "var", typeId, dimids);
  assert_checkreturn(ret, 0, "nc_def_var(...)");

  arrayIn = mat2array(values, typeName);

  cmd = msprintf("ret = nc_put_var_%s(ncid, varid, arrayIn);", lowlevel_function_suffix(typeName));
  execstr(cmd);
  assert_checkreturn(ret, 0, cmd);

  deleteArray(arrayIn, typeName, nbValues);

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");

  [ret, ncid] = nc_open(filepath, NC_NOWRITE);
  assert_checkreturn(ret, 0, "nc_open(...)");

  [ret, varid] = nc_inq_varid(ncid, "var");
  assert_checkreturn(ret, 0, "nc_inq_varid(...)");

  arrayOut = newArray(typeName, nbValues);

  cmd = msprintf("ret = nc_get_var_%s(ncid, varid, arrayOut);", lowlevel_function_suffix(typeName));
  execstr(cmd);
  assert_checkreturn(ret, 0, cmd);

  valuesOut = array2mat(arrayOut, nbValues, typeName);
  assert_checkequal(valuesOut, values(1:$));

  deleteArray(arrayOut, typeName, nbValues);

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...);");
endfunction

value = [1 2; 3 4];
test_put_get_var_type("schar", NC_BYTE, value);
test_put_get_var_type("uchar", NC_UBYTE, value);
test_put_get_var_type("short", NC_SHORT, value);
test_put_get_var_type("ushort", NC_USHORT, value);
test_put_get_var_type("int", NC_INT, value);
test_put_get_var_type("uint", NC_UINT, value);
test_put_get_var_type("uchar", NC_FLOAT, value);
test_put_get_var_type("double", NC_DOUBLE, value);

test_put_get_var_type("char", NC_CHAR, "abcd");
test_put_get_var_type("string", NC_STRING, ["aa" "bb"; "cc" "dd"]);

