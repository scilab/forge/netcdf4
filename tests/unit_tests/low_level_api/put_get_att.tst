scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function filepath = test_put_get_att_type(typeName, typeId, values)
  filepath = fullfile(TMPDIR, msprintf("put_get_att_%s.nc", typeName));
  [ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
  assert_checkreturn(ret, 0, "nc_create(...)");

  arrayIn = mat2array(values, typeName);

  if typeName <> "char" then
    nbvalues = size(values, '*');
    cmd = msprintf("ret = nc_put_att_%s(ncid, NC_GLOBAL, ""att"", typeId, nbvalues, arrayIn)", typeName);
  else
    nbvalues = length(values);
    cmd = "ret = nc_put_att_text(ncid, NC_GLOBAL, ""att"", nbvalues, arrayIn)";
  end
  execstr(cmd);
  assert_checkreturn(ret, 0, cmd);

  deleteArray(arrayIn, typeName);

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");

  [ret, ncid] = nc_open(filepath, NC_NOWRITE);
  assert_checkreturn(ret, 0, "nc_open(...)");

  [ret, attlen] = nc_inq_attlen(ncid, NC_GLOBAL, "att");
  assert_checkreturn(ret, 0, cmd);

  arrayOut = newArray(typeName, attlen);
  if typeName <> "char" then
    cmd = msprintf("ret = nc_get_att_%s(ncid, NC_GLOBAL, ""att"", arrayOut)", typeName);
  else
    cmd = "ret = nc_get_att_text(ncid, NC_GLOBAL, ""att"", arrayOut)";
  end
  execstr(cmd);
  assert_checkreturn(ret, 0, cmd);

  valuesOut = array2mat(arrayOut, attlen, typeName);
  assert_checkequal(valuesOut', values);

  deleteArray(arrayOut, typeName);

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");
endfunction

test_put_get_att_type("char", NC_CHAR, "abcd");

value = [1 2 3 4];
test_put_get_att_type("schar", NC_BYTE, value);
test_put_get_att_type("uchar", NC_UBYTE, value);
test_put_get_att_type("short", NC_SHORT, value);
test_put_get_att_type("ushort", NC_USHORT, value);
test_put_get_att_type("int", NC_INT, value);
test_put_get_att_type("uint", NC_UINT, value);
test_put_get_att_type("float", NC_FLOAT, value);
test_put_get_att_type("double", NC_DOUBLE, value);

//test_put_get_att_type("string", NC_STRING, ["aa" "bb"; "cc" "dd"]);
