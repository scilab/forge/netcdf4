scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = fullfile(scinetcdf_path, "tests/unit_tests/data/nc_inq.nc");

[ret, ncid] = nc_open(filepath, NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

[ret, dimids] = nc_inq_dimids(ncid, 0);
assert_checkreturn(ret, 0, "nc_inq_dimids(...)");
assert_checkequal(dimids, 0:3);

dim2id = 1;

[ret, dimname, dimlen] = nc_inq_dim(ncid, dim2id);
assert_checkreturn(ret, 0, "nc_inq_dim(...)");
assert_checkequal(dimlen, 2);

[ret, dimid] = nc_inq_dimid(ncid, "dim2");
assert_checkreturn(ret, 0, "nc_inq_dimid(...)");
assert_checkequal(dimid, dim2id);

[ret, dimname] = nc_inq_dimname(ncid, dim2id);
assert_checkreturn(ret, 0, "nc_inq_dimname(...)");
assert_checkequal(dimname, "dim2");

[ret, dimlen] = nc_inq_dimlen(ncid, dim2id);
assert_checkreturn(ret, 0, "nc_inq_dimlen(...)");
assert_checkequal(dimlen, 2);

dim4id = 3;
[ret, dimlen] = nc_inq_dimlen(ncid, dim4id);
assert_checkreturn(ret, 0, "nc_inq_dimlen(...)");
assert_checkequal(int32(dimlen), NC_UNLIMITED);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");



