content_header = [ ..
"netcdf micro {"; ..
"variables:"; ..
"   int n ;"; ..
];

content_var_full = [ ..
"data:"; ..
""; ..
"   n = 10 ;"; ..
];

content_grp_min = [ ..
""; ..
"group: grp {"; ..
"  variables:"; ..
"  	double x ;"; ..
"  } // group grp" ..
];

content_grp_full = [ ..
""; ..
"group: grp {"; ..
"  variables:"; ..
"  	double x ;"; ..
"  data:"; ..
""; ..
"     x = 1 ;"; ..
"  } // group grp" ..
];

content_grp_var_full = [ ..
""; ..
"group: grp {"; ..
"  variables:"; ..
"  	double x ;"; ..
"  data:"; ..
"  } // group grp" ..
];

content_footer =  [ ..
"}" ..
];

function check_ncdisp(location, disp_format, expected_content)
  content = ncdisp(filepath, location, disp_format);
  assert_checkequal(stripblanks(content, %t), stripblanks(expected_content, %t));
endfunction

scinetcdf_path = getSciNetCDFPath();
filepath = fullfile(scinetcdf_path, 'tests/unit_tests/data/micro.nc');

exp_content = [content_header; content_var_full; content_grp_full; content_footer];
check_ncdisp('/', 'to_var', exp_content);

exp_content_min = [content_header; content_grp_min; content_footer];
check_ncdisp('/', ['min', 'to_var'], exp_content_min);

exp_content_var_full = [content_header; content_var_full; content_grp_var_full; content_footer];
check_ncdisp('n', 'to_var', exp_content_var_full);

exp_content_grp_full = [content_header; content_grp_full; content_footer];
check_ncdisp('grp/', 'to_var', exp_content_grp_full);

// Check errors
expected_errmsg = "ncdisp: /notfound.nc: No such file or directory.";
assert_checkerror("ncdisp(""/notfound.nc"")", expected_errmsg);

