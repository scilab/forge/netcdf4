function cflags = getCompilationFlags()
    os = getos();
    [version, opts] = getversion();
    arch = opts(2);
    cflags = "-I" + fullpath(fullfile("../../thirdparty", os, arch, "include"));
    cflags = cflags + " -I" + fullpath("../../src/c");
    if os == "Windows" then
        cflags = cflags + " -DDLL_NETCDF";
    else
        cflags = cflags + " -g";
    end
endfunction


function ldflags = getLinkFlags()
    os = getos();
    [version, opts] = getversion();
    arch = opts(2);
    thirdparty_lib = fullpath(fullfile("../../thirdparty", os, arch, "lib"));
    if getos() == "Windows" then
        if findmsvccompiler() <> "unknown" then
            // Visual Studio
            ldflags = fullpath("../../src/c/libscinetcdf_hl.lib");
            ldflags = ldflags + " " + fullfile(thirdparty_lib, "netcdf.lib");
        else
            // MinGW
            ldflags = msprintf("-L%s -lscinetcdf_hl", fullpath("../../src/c"));
            ldflags = ldflags + " " + msprintf("-L%s -lnetcdf", thirdparty_lib);
        end
    else
        ldflags = msprintf("-L%s -lscinetcdf_hl", fullpath("../../src/c"));
        ldflags = " " + msprintf("-L%s -lnetcdf", thirdparty_lib);
    end
endfunction

