%{
#include "scinetcdf_hl.h"
%}

%typemap(in) (...) (st_option_list option_list)
{
  int nbOptionalArgs;

  nbOptionalArgs = SWIG_NbInputArgument(pvApiCtx) - 2;
  if (nbOptionalArgs > 0) {
    st_option *options;
    st_option *option;
    int i;

    if (nbOptionalArgs % 2 != 0) {
      Scierror(999, _("%s: Optional arguments are pairs of a name and a value.\n"), fname);
      return SWIG_ERROR;
    }
    option_list.nboptions = nbOptionalArgs / 2;

    options = (st_option*) malloc(option_list.nboptions * sizeof(st_option));
    option = options;

    for (i=0; i<option_list.nboptions; i++) {
      SciErr sciErr;
      int *piAddrVar;
      int iType;
      char *pstName = NULL;

      sciErr = getVarAddressFromPosition(pvApiCtx, 2*i+3, &piAddrVar);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return SWIG_ERROR;
      }

      sciErr = getVarType(pvApiCtx, piAddrVar, &iType);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return SWIG_ERROR;
      }

      if (iType != sci_strings) {
        Scierror(999, _("%s: Wrong type for input argument #%d: A string is expected.\n"), fname, i);
        return SWIG_ERROR;
      }

      if (getAllocatedSingleString(pvApiCtx, piAddrVar, &pstName)) {
        return SWIG_ERROR;
      }

      option->name = pstName;

      sciErr = getVarAddressFromPosition(pvApiCtx, 2*i+4, &piAddrVar);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return SWIG_ERROR;
      }

      sciErr = getVarType(pvApiCtx, piAddrVar, &iType);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return SWIG_ERROR;
      }

      if (iType == sci_matrix) {
        option->value = (double *) malloc(sizeof(double));
        if (getScalarDouble(pvApiCtx, piAddrVar, (double *)option->value)) {
            return SWIG_ERROR;
        }
        option->type = OT_DOUBLE;
      }
      else if (iType == sci_ints) {
        int iPrec;
        sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddrVar, &iPrec);
        if(sciErr.iErr) {
            printError(&sciErr, 0);
            return sciErr.iErr;
        }
        switch(iPrec) {
          case SCI_INT8 : {
            option->value = (signed char *) malloc(sizeof(signed char));
            if (getScalarInteger8(pvApiCtx, piAddrVar, (signed char*)option->value)) {
                return SWIG_ERROR;
            }
            break;
          }
          case SCI_INT16 : {
            option->value = (short *) malloc(sizeof(short));
            if (getScalarInteger16(pvApiCtx, piAddrVar, (short*)option->value )) {
                return SWIG_ERROR;
            }
            break;
          }
          case SCI_INT32 : {
            option->value = (int *) malloc(sizeof(int));
            if (getScalarInteger32(pvApiCtx, piAddrVar, (int*)option->value)) {
                return SWIG_ERROR;
            }
            break;
          }
          case SCI_UINT8 : {
            option->value = (unsigned char *) malloc(sizeof(unsigned char));
            if (getScalarUnsignedInteger8(pvApiCtx, piAddrVar, (unsigned char*)option->value)) {
                return SWIG_ERROR;
            }
            break;
          }
          case SCI_UINT16 : {
            option->value = (unsigned short *) malloc(sizeof(unsigned short));
            if (getScalarUnsignedInteger16(pvApiCtx, piAddrVar, (unsigned short*)option->value)) {
                return SWIG_ERROR;
            }
            break;
          }
          case SCI_UINT32 : {
            option->value = (unsigned int *) malloc(sizeof(unsigned int));
            if (getScalarUnsignedInteger32(pvApiCtx, piAddrVar, (unsigned int*)option->value)) {
                return SWIG_ERROR;
            }
            break;
          }
        }
        option->type = (option_type)((int)OT_INT8 + iPrec - 1);
      }
      else if (iType == sci_strings) {
        if (getAllocatedSingleString(pvApiCtx, piAddrVar, (char **)&(option->value))) {
            return SWIG_ERROR;
        }
        option->type = OT_STRING;
      }
      else if ((iType == sci_list) || (iType == sci_mlist) || (iType == sci_tlist)) {
        int iListSize;
        st_option_list *child_option_list;
        st_option *child_options;
        st_option *child_option;
        int j;

        child_option_list = (st_option_list*) malloc(sizeof(st_option_list));

        sciErr = getListItemNumber(pvApiCtx, piAddrVar, &iListSize);
        if (sciErr.iErr) {
          printError(&sciErr, 0);
          return SWIG_ERROR;
        }

        child_option_list->nboptions = iListSize / 2;
        child_options = (st_option*) malloc(child_option_list->nboptions * sizeof(st_option));
        child_option = child_options;

        for (j=0; j<child_option_list->nboptions; j++) {
          int *piAddrItem = NULL;
          int iItemType;
          int iCols;
          int iRows;
          double *pdData;
          char *pstName = NULL;

          sciErr = getListItemAddress(pvApiCtx, piAddrVar, 2*j+1, &piAddrItem);
          if (sciErr.iErr) {
            printError(&sciErr, 0);
            return SWIG_ERROR;
          }

          sciErr = getVarType(pvApiCtx, piAddrItem, &iItemType);
          if (sciErr.iErr) {
            printError(&sciErr, 0);
            return SWIG_ERROR;
          }

          if (iItemType != sci_strings) {
            Scierror(999, _("%s: Wrong type for item #%d: A string is expected.\n"), fname, 2*j+1);
            return SWIG_ERROR;
          }

          if (getAllocatedSingleString(pvApiCtx, piAddrItem, &pstName)) {
            return SWIG_ERROR;
          }

          child_option->name = pstName;

          sciErr = getListItemAddress(pvApiCtx, piAddrVar, 2*j+2, &piAddrItem);
          if (sciErr.iErr) {
            printError(&sciErr, 0);
            return SWIG_ERROR;
          }

          sciErr = getVarType(pvApiCtx, piAddrItem, &iItemType);
          if (sciErr.iErr) {
            printError(&sciErr, 0);
            return SWIG_ERROR;
          }

          if (iItemType == sci_matrix) {
            sciErr = getMatrixOfDoubleInList(pvApiCtx, piAddrVar, 2*j+2, &iRows, &iCols, &pdData);
            if (sciErr.iErr) {
                printError(&sciErr, 0);
                return SWIG_ERROR;
            }

            if (iRows * iCols > 1) {
                Scierror(999, _("%s: Wrong size for item #%d: A scalar is expected.\n"), fname, 2*j+2);
                return SWIG_ERROR;
            }
            child_option->type = OT_DOUBLE;
            child_option->value = (double *) malloc(sizeof(double));
            *((double*)child_option->value) = *pdData;
          }
          child_option++;
        }
        child_option_list->options = child_options;
        option->type = OT_LIST;
        option->value = child_option_list;
      }
      option++;
    }
    option_list.options = options;
  }
  else {
    option_list.nboptions = 0;
    option_list.options = NULL;
  }

  $1 = &option_list;
}

%typemap(in, numinputs=0, noblock=1) st_data *data (st_data tmp_data)
{
  $1 = &tmp_data;
}

%typemap(argout) st_data *data
{
  int *piDims = NULL;
  size_t size;
  int nbdims;
  int i;

  size = getHypermatSize($1->dims, $1->nbdims);
  if ($1->nbdims > 2) {
    SciErr sciErr;
    sciErr.iErr = 0;

    switch($1->type) {
      case NC_BYTE: {
        signed char *pscColumnMajorData = (signed char *) malloc(size * sizeof(signed char));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pscColumnMajorData, sizeof(signed char));
        sciErr = createHypermatOfInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, pscColumnMajorData);
        break;
      }
      case NC_UBYTE: {
        unsigned char *pucColumnMajorData = (unsigned char *) malloc(size * sizeof(unsigned char));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pucColumnMajorData, sizeof(unsigned char));
        sciErr = createHypermatOfUnsignedInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, pucColumnMajorData);
        break;
      }
      case NC_SHORT: {
        short *psColumnMajorData = (short *) malloc(size * sizeof(short));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, psColumnMajorData, sizeof(short));
        sciErr = createHypermatOfInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, psColumnMajorData);
        break;
      }
      case NC_USHORT: {
        unsigned short *pusColumnMajorData = (unsigned short *) malloc(size * sizeof(unsigned short));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pusColumnMajorData, sizeof(unsigned short));
        sciErr = createHypermatOfUnsignedInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, pusColumnMajorData);
        break;
      }
      case NC_INT: {
        int *piColumnMajorData = (int *) malloc(size * sizeof(int));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, piColumnMajorData, sizeof(int));
        sciErr = createHypermatOfInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, piColumnMajorData);
        break;
      }
      case NC_UINT: {
        unsigned int *puiColumnMajorData = (unsigned int *) malloc(size * sizeof(unsigned int));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, puiColumnMajorData, sizeof(unsigned int));
        sciErr = createHypermatOfUnsignedInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, puiColumnMajorData);
        break;
      }
      case NC_FLOAT:
      case NC_DOUBLE: {
        double *pdColumnMajorData = (double *) malloc(size * sizeof(double));
        ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pdColumnMajorData, sizeof(double));
        sciErr = createHypermatOfDouble(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), piDims, $1->nbdims, pdColumnMajorData);
        break;
      }
    }
    if (sciErr.iErr) {
      printError(&sciErr, 0);
      return 0;
    }
  }
  else if ($1->nbdims > 0) {
    if ($1->type != NC_CHAR) {
      SciErr sciErr;
      sciErr.iErr = 0;

      switch($1->type) {
        case NC_BYTE: {
          signed char *pscColumnMajorData = (signed char *) malloc(size * sizeof(signed char));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pscColumnMajorData, sizeof(signed char));
          sciErr = createMatrixOfInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], pscColumnMajorData);
          break;
        }
        case NC_UBYTE: {
          unsigned char *pucColumnMajorData = (unsigned char *) malloc(size * sizeof(unsigned char));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pucColumnMajorData, sizeof(unsigned char));
          sciErr = createMatrixOfUnsignedInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], pucColumnMajorData);
          break;
        }
        case NC_SHORT: {
          short *psColumnMajorData = (short *) malloc(size * sizeof(short));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, psColumnMajorData, sizeof(short));
          sciErr = createMatrixOfInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], psColumnMajorData);
          break;
        }
        case NC_USHORT: {
          unsigned short *pusColumnMajorData = (unsigned short *) malloc(size * sizeof(unsigned short));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pusColumnMajorData, sizeof(unsigned short));
          sciErr = createMatrixOfUnsignedInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], pusColumnMajorData);
          break;
        }
        case NC_INT: {
          int *piColumnMajorData = (int *) malloc(size * sizeof(int));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, piColumnMajorData, sizeof(int));
          sciErr = createMatrixOfInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], piColumnMajorData);
          break;
        }
        case NC_UINT: {
          unsigned int *puiColumnMajorData = (unsigned int *) malloc(size * sizeof(unsigned int));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, puiColumnMajorData, sizeof(unsigned int));
          sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], puiColumnMajorData);
          break;
        }
        case NC_FLOAT:
        case NC_DOUBLE: {
          double *pdColumnMajorData = (double *) malloc(size * sizeof(double));
          ncToSciData($1->dims, $1->nbdims, &piDims, $1->data, pdColumnMajorData, sizeof(double));
          sciErr = createMatrixOfDouble(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
            SWIG_Scilab_GetOutputPosition(), piDims[0], piDims[1], pdColumnMajorData);
          break;
        }
      }
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }
    }
    else {
      int iRet;
      if ((iRet = createSingleString(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), (const char*)$1->data))) {
        return iRet;
      }
    }
  }
  else {
    int ret;
    double dvalue;
    switch($1->type) {
      case NC_BYTE: {
        dvalue = (double) *((signed char*)$1->data);
        break;
      }
      case NC_UBYTE: {
        dvalue = (double) *((unsigned char*)$1->data);
        break;
      }
      case NC_SHORT: {
        dvalue = (double) *((short*)$1->data);
        break;
      }
      case NC_USHORT: {
        dvalue = (double) *((unsigned short*)$1->data);
        break;
      }
      case NC_INT: {
        dvalue = (double) *((int*)$1->data);
        break;
      }
      case NC_UINT: {
        dvalue = (double) *((unsigned int*)$1->data);
        break;
      }
      case NC_FLOAT:
      case NC_DOUBLE: {
        dvalue = (double) *((double*)$1->data);
        break;
      }
    }
    if ((ret = createScalarDouble(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
      SWIG_Scilab_GetOutputPosition(), dvalue))) {
      return ret;
    }
  }
  SWIG_Scilab_SetOutput(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition());
}

%typemap(in) st_data data
{
  SciErr sciErr;
  int *piAddrVar = NULL;
  int iTypeSize;
  void *pvColumnMajorData = NULL;
  char *pstData;

  // Argument $input is a hypermatrix or a matrix
  sciErr = getVarAddressFromPosition(pvApiCtx, $input, &piAddrVar);
  if (sciErr.iErr) {
    printError(&sciErr, 0);
    return sciErr.iErr;
  }

  if (isHypermatType(pvApiCtx, piAddrVar)) {
    int *piDims = NULL;
    int htype = 0;
    int iPrec = 0;
    int size;

    sciErr = getHypermatType(pvApiCtx, piAddrVar, &htype);
    if (sciErr.iErr) {
      printError(&sciErr, 0);
      return sciErr.iErr;
    }

    if (htype == sci_ints) {
      sciErr = getHypermatOfIntegerPrecision(pvApiCtx, piAddrVar, &iPrec);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }

      switch (iPrec) {
        case SCI_UINT8: {
          $1.type = NC_UBYTE;
          iTypeSize = sizeof(unsigned char);
          sciErr = getHypermatOfUnsignedInteger8(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (unsigned char**)&pvColumnMajorData);
          break;
        }
        case SCI_INT8: {
          $1.type = NC_BYTE;
          iTypeSize = sizeof(unsigned char);
          sciErr = getHypermatOfInteger8(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (char**)&pvColumnMajorData);
          iTypeSize = sizeof(signed char);
          break;
        }
        case SCI_UINT16: {
          $1.type = NC_USHORT;
          iTypeSize = sizeof(unsigned short);
          sciErr = getHypermatOfUnsignedInteger16(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (unsigned short **)&pvColumnMajorData);
          break;
        }
        case SCI_INT16: {
          $1.type = NC_SHORT;
          iTypeSize = sizeof(short);
          sciErr = getHypermatOfInteger16(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (short **)&pvColumnMajorData);
          break;
        }
        case SCI_UINT32: {
          $1.type = NC_UINT;
          iTypeSize = sizeof(unsigned int);
          sciErr = getHypermatOfUnsignedInteger32(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (unsigned int **)&pvColumnMajorData);
          break;
        }
        case SCI_INT32: {
          $1.type = NC_INT;
          iTypeSize = sizeof(int);
          sciErr = getHypermatOfInteger32(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (int **)&pvColumnMajorData);
          break;
        }
      }
    }
    else if (htype == sci_matrix) {
      $1.type = NC_DOUBLE;
      iTypeSize = sizeof(double);
      sciErr = getHypermatOfDouble(pvApiCtx, piAddrVar, &piDims, &$1.nbdims, (double **)&pvColumnMajorData);
    }
    else {
      Scierror(999, _("%s: Wrong type for input argument #%d: A hypermatrix of double or integer is expected.\n"), fname, $input);
      return SWIG_ERROR;
    }
    if (sciErr.iErr) {
      printError(&sciErr, 0);
      return sciErr.iErr;
    }

    sciToNcData(piDims, $1.nbdims, &$1.dims, pvColumnMajorData, &$1.data, iTypeSize);
  }
  else if (isVarMatrixType(pvApiCtx, piAddrVar)) {
    int piDims[2];
    int iPrec = 0;

    if (isIntegerType(pvApiCtx, piAddrVar)) {
      sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddrVar, &iPrec);
      if(sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }

      switch (iPrec) {
        case SCI_UINT8: {
          $1.type = NC_UBYTE;
          iTypeSize = sizeof(unsigned char);
          sciErr = getMatrixOfUnsignedInteger8(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (unsigned char**)&pvColumnMajorData);
          break;
        }
        case SCI_INT8: {
          $1.type = NC_BYTE;
          iTypeSize = sizeof(signed char);
          sciErr = getMatrixOfInteger8(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (char**)&pvColumnMajorData);
          break;
        }
        case SCI_UINT16: {
          $1.type = NC_USHORT;
          iTypeSize = sizeof(unsigned short);
          sciErr = getMatrixOfUnsignedInteger16(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (unsigned short**)&pvColumnMajorData);
          break;
        }
        case SCI_INT16: {
          $1.type = NC_SHORT;
          iTypeSize = sizeof(short);
          sciErr = getMatrixOfInteger16(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (short**)&pvColumnMajorData);
          break;
        }
        case SCI_UINT32: {
          $1.type = NC_UINT;
          iTypeSize = sizeof(unsigned int);
          sciErr = getMatrixOfUnsignedInteger32(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (unsigned int**)&pvColumnMajorData);
          break;
        }
        case SCI_INT32: {
          $1.type = NC_INT;
          iTypeSize = sizeof(int);
          sciErr = getMatrixOfInteger32(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (int**)&pvColumnMajorData);
          break;
        }
      }
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }
    }
    else if (isDoubleType(pvApiCtx, piAddrVar)) {
      $1.type = NC_DOUBLE;
      iTypeSize = sizeof(double);
      sciErr = getMatrixOfDouble(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], (double**)&pvColumnMajorData);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }
    }
    else if (isStringType(pvApiCtx, piAddrVar)) {
      sciErr = getMatrixOfString(pvApiCtx, piAddrVar, &piDims[0], &piDims[1], NULL, NULL);
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }

      if ((piDims[0] == 1) && (piDims[1] == 1)) {
        int iRet;
        $1.type = NC_CHAR;
        iTypeSize = sizeof(char);
        if ((iRet = getAllocatedSingleString(pvApiCtx, piAddrVar, (char**)&pstData))) {
          return iRet;
        }
      }
      else {
        Scierror(999, _("%s: Wrong type for input argument #%d: A matrix of integer/double or a string is expected.\n"), fname, $input);
        return SWIG_ERROR;
      }
    }
    else {
      Scierror(999, _("%s: Wrong type for input argument #%d: A matrix of integer/double or a string is expected.\n"), fname, $input);
      return sciErr.iErr;
    }

    if ($1.type != NC_CHAR) {
      if ((piDims[0] > 1) || (piDims[1] > 1)) {
        $1.nbdims = 2;
        sciToNcData(&piDims[0], $1.nbdims, &$1.dims, pvColumnMajorData, &$1.data, iTypeSize);
      }
	  else {
	    $1.nbdims = 0;
	    $1.data = pvColumnMajorData;
      }
    }
    else {
      $1.nbdims = 1;
      $1.dims = (size_t*) malloc(sizeof(size_t));
      $1.dims[0] = strlen(pstData)+1;
      $1.data = pstData;
    }
  }
  else {
    Scierror(999, _("%s: Wrong type for input argument #%d: A hypermatrix or a matrix is expected.\n"), fname, $input);
    return sciErr.iErr;
  }
}

%typemap(in, numinputs=0, noblock=1) st_attdata *attdata (st_attdata tmp_attdata)
{
  $1 = &tmp_attdata;
}

%typemap(argout) st_attdata *attdata
{
  if ($1->len > 0) {
    SciErr sciErr;
    sciErr.iErr = 0;

    switch($1->type) {
      case NC_BYTE: {
        sciErr = createMatrixOfInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_UBYTE: {
        sciErr = createMatrixOfUnsignedInteger8(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_SHORT: {
        sciErr = createMatrixOfInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_USHORT: {
        sciErr = createMatrixOfUnsignedInteger16(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_INT: {
        sciErr = createMatrixOfInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_UINT: {
        sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_FLOAT: {
        // TODO
        break;
      }
      case NC_DOUBLE: {
        sciErr = createMatrixOfDouble(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), 1, $1->len, $1->data);
        break;
      }
      case NC_CHAR: {
        int ret;
        if ((ret = createSingleString(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) +
          SWIG_Scilab_GetOutputPosition(), $1->data))) {
          return ret;
        }
      }
    }
    if (sciErr.iErr) {
      printError(&sciErr, 0);
      return 0;
    }
  }
  else {
    int iRet;
    if ((iRet = createEmptyMatrix(pvApiCtx, nbInputArgument(pvApiCtx) + 1))) {
        return iRet;
    }
  }

  SWIG_Scilab_SetOutput(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition());
}

%typemap(in) st_attdata attdata
{
  SciErr sciErr;
  int *piAddrVar = NULL;
  int iTypeSize;

  // Argument $input is a matrix
  sciErr = getVarAddressFromPosition(pvApiCtx, $input, &piAddrVar);
  if (sciErr.iErr) {
    printError(&sciErr, 0);
    return sciErr.iErr;
  }

  if (isVarMatrixType(pvApiCtx, piAddrVar)) {
    int iPrec = 0;
    int iRows = 0;
    int iCols = 0;

    if (isIntegerType(pvApiCtx, piAddrVar)) {
      sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddrVar, &iPrec);
      if(sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }

      switch (iPrec) {
        case SCI_UINT8: {
          $1.type = NC_UBYTE;
          sciErr = getMatrixOfUnsignedInteger8(pvApiCtx, piAddrVar, &iRows, &iCols, (unsigned char**)&$1.data);
          break;
        }
        case SCI_INT8: {
          $1.type = NC_BYTE;
          sciErr = getMatrixOfInteger8(pvApiCtx, piAddrVar, &iRows, &iCols, (char**)&$1.data);
          break;
        }
        case SCI_UINT16: {
          $1.type = NC_USHORT;
          sciErr = getMatrixOfUnsignedInteger16(pvApiCtx, piAddrVar, &iRows, &iCols, (unsigned short**)&$1.data);
          break;
        }
        case SCI_INT16: {
          $1.type = NC_SHORT;
          sciErr = getMatrixOfInteger16(pvApiCtx, piAddrVar, &iRows, &iCols, (short**)&$1.data);
          break;
        }
        case SCI_UINT32: {
          $1.type = NC_UINT;
          sciErr = getMatrixOfUnsignedInteger32(pvApiCtx, piAddrVar, &iRows, &iCols, (unsigned int**)&$1.data);
          break;
        }
        case SCI_INT32: {
          $1.type = NC_INT;
          sciErr = getMatrixOfInteger32(pvApiCtx, piAddrVar, &iRows, &iCols, (int**)&$1.data);
          break;
        }
      }
      if (sciErr.iErr) {
        printError(&sciErr, 0);
        return sciErr.iErr;
      }
      $1.len = iRows * iCols;
    }
    else if (isDoubleType(pvApiCtx, piAddrVar)) {
      $1.type = NC_DOUBLE;
      sciErr = getMatrixOfDouble(pvApiCtx, piAddrVar, &iRows, &iCols, (double**)&$1.data);
      $1.len = iRows * iCols;
    }
    else if (isStringType(pvApiCtx, piAddrVar)) {
      int ret;
      $1.type = NC_CHAR;
      if ((ret = getAllocatedSingleString(pvApiCtx, piAddrVar, (char**)&$1.data))) {
        return ret;
      }
      $1.len = strlen((char*)$1.data);
    }
    else {
      Scierror(999, _("%s: Wrong type for input argument #%d: A matrix or a string is expected.\n"), SWIG_Scilab_GetFuncName(), $input);
    }
  }
  else {
    Scierror(999, _("%s: Wrong type for input argument #%d: A matrix or a string is expected.\n"), SWIG_Scilab_GetFuncName(), $input);
  }
}

%typemap(in, fragment="SWIG_SciDoubleOrInt32_AsIntArrayAndSize") size_t *start
{
  int iRows = 0;
  int iCols = 0;
  int *piStart = NULL;
  if (SWIG_SciDoubleOrInt32_AsIntArrayAndSize(pvApiCtx, $input, &iRows, &iCols, &piStart, SWIG_Scilab_GetFuncName()) == SWIG_OK) {
    $1 = (size_t *) malloc(iRows * iCols * sizeof(size_t));
    sciToNcDims(piStart, iRows * iCols, $1);
  }
  else {
    return SWIG_ERROR;
  }
}

%typemap(in, fragment="SWIG_SciDoubleOrInt32_AsIntArrayAndSize") size_t *count
{
  int iRows = 0;
  int iCols = 0;
  int *piCount = NULL;
  if (SWIG_SciDoubleOrInt32_AsIntArrayAndSize(pvApiCtx, $input, &iRows, &iCols, &piCount, SWIG_Scilab_GetFuncName()) == SWIG_OK) {
    $1 = (size_t *) malloc(iRows * iCols * sizeof(size_t));
    sciToNcDims(piCount, iRows * iCols, $1);
  }
  else {
    return SWIG_ERROR;
  }
}

%typemap(in, fragment="SWIG_SciDoubleOrInt32_AsIntArrayAndSize") ptrdiff_t *stride
{
  int iRows = 0;
  int iCols = 0;
  int *piStride = NULL;
  if (SWIG_SciDoubleOrInt32_AsIntArrayAndSize(pvApiCtx, $input, &iRows, &iCols, &piStride, SWIG_Scilab_GetFuncName()) == SWIG_OK) {
    int iNbStride = iRows * iCols;
    if (iNbStride > 0) {
      $1 = (ptrdiff_t*) malloc(iNbStride * sizeof(ptrdiff_t));
      if (iNbStride > 1) {
        int i;
        $1[iNbStride-2] = piStride[0];
        $1[iNbStride-1] = piStride[1];
        for (i=0; i<iNbStride-2; i++) {
          $1[i] = piStride[iNbStride-i-1];
        }
      }
      else {
        $1[0] = piStride[0];
      }
    }
    else {
      $1 = NULL;
    }
  }
  else {
    return SWIG_ERROR;
  }
}

%typemap(in, numinputs=0, noblock=1) char **errmsg (char *tmpErrMsg = NULL)
{
  $1 = &tmpErrMsg;
}

%typemap(freearg, noblock=1, noblock=1) char **errmsg
{
  if (*$1 != NULL) {
    char errMsg[256];
    sprintf(errMsg, "%s: %s\n", SWIG_Scilab_GetFuncName(), *$1);
    Scierror(999, errMsg);
    free(*$1);
  }
}

%rename(nccreate) imp_nccreate;
%rename(ncread) imp_ncread;
%rename(ncwrite) imp_ncwrite;
%rename(ncwriteatt) imp_ncwriteatt;
%rename(ncreadatt) imp_ncreadatt;

%apply int *OUTPUT { int *errcode };

void imp_nccreate(char *filename, char *fullvarname, char **errmsg, ...);
void imp_ncwrite(char *filename, char *fullvarname, st_data data, char **errmsg, size_t *start = NULL, ptrdiff_t *stride = NULL);
void imp_ncread(char *filename, char *fullvarname, st_data *data, char **errmsg, size_t *start = NULL, size_t *count = NULL, ptrdiff_t *stride = NULL);
void imp_ncwriteatt(char *filename, char *location, char *attname, st_attdata attdata, char **errmsg);
void imp_ncreadatt(char *source, char *location, char *attname, st_attdata *attdata, char **errmsg);

