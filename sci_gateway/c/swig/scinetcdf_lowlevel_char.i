// Typemap for output pre-allocated char* typemap

%typemap(in, noblock=1, numinputs=0) char **OUTPUT (char* buf)
{
  $1 = &buf;
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") char **OUTPUT
{
  %set_output(SWIG_FromCharPtr(*$1));
}

// Typemap for use char* as pointer (instead of default as string)

%typemap(in, noblock=1) char const *op (char *argp, int res = 0)
{
  res = SWIG_ConvertPtr($input, &argp, $descriptor(void *), %convertptr_flags);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum);
  }
  $1 = (char *)argp;
}

%typemap(in, noblock=1) char *ip = const char* op;
