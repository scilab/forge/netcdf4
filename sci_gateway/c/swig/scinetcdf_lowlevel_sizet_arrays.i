%fragment("SWIG_SciDoubleOrInt32_AsSizeArrayAndSize", "header", fragment="SWIG_SciDoubleOrInt32_AsIntArrayAndSize") %{
SWIGINTERN int
SWIG_SciDoubleOrInt32_AsSizeArrayAndSize(void *pvApiCtx, SwigSciObject iVar, size_t **pszData, int *piNbData, char *fname)
{
  int iRow = 0;
  int iCol = 0;
  int *piData = NULL;
  if (SWIG_SciDoubleOrInt32_AsIntArrayAndSize(pvApiCtx, iVar, &iRow, &iCol, &piData, fname) == SWIG_OK) {
    int i;
    *piNbData = iRow * iCol;
    *pszData = (size_t*) malloc(*piNbData * sizeof(size_t));
    for (i = 0; i < *piNbData; i++) {
      (*pszData)[i] = (size_t) piData[i];
    }
    return SWIG_OK;
  }
  else {
    return SWIG_ERROR;
  }
}
%}

%typemap(in, noblock=1, fragment="SWIG_SciDoubleOrInt32_AsSizeArrayAndSize") size_t *INPUT (int res, int iDummy)
{
  res = SWIG_SciDoubleOrInt32_AsSizeArrayAndSize(pvApiCtx, $input, &$1, &iDummy, SWIG_Scilab_GetFuncName());
  if (!SWIG_IsOK(res)) {
    return SWIG_ERROR;
  }
}

%typemap(in, noblock=1, fragment="SWIG_AsCharPtrAndSize") char **INPUT (char *buf)
{
  if (SWIG_AsCharPtrAndSize($input, &buf, NULL, 0) == SWIG_OK) {
    $1 = &buf;
  }
  else {
    return SWIG_ERROR;
  }
}
