// Numeric arrays

%include <carrays.i>

%array_functions(signed char, SCharArray);
%array_functions(unsigned char, UCharArray);
%array_functions(short, ShortArray);
%array_functions(unsigned short, UShortArray);
%array_functions(int, IntArray);
%array_functions(unsigned int, UIntArray);
%array_functions(long, LongArray);
%array_functions(float, FloatArray);
%array_functions(double, DoubleArray);


// Char array

%{
static void *new_CharArray(size_t iCount) {
  return malloc(iCount * sizeof(char));
}

static void delete_CharArray(void *pvCharArray) {
  free(pvCharArray);
}

static char CharArray_getitem(void *pvCharArray, size_t iIndex) {
  return ((char *)pvCharArray)[iIndex];
}
static void CharArray_setitem(void *pvCharArray, size_t iIndex, char cValue) {
  ((char *)pvCharArray)[iIndex] = cValue;
}
%}

void *new_CharArray(size_t iCount);
void delete_CharArray(void *pvCharArray);
char CharArray_getitem(void *pvCharArray, size_t iIndex);
void CharArray_setitem(void *pvCharArray, size_t iIndex, char cValue);


// String array

%{
static char **new_StringArray(size_t iCount) {
  return (char**) malloc(iCount * sizeof(char*));
}

static void delete_StringArray(char **pstStringArray, int iCount) {
  int i;
  for (i=1; i<iCount; i++)
      free(pstStringArray[i]);
  free(pstStringArray);
}

static char* StringArray_getitem(char **pstStringArray, size_t iIndex) {
  return pstStringArray[iIndex];
}
static void StringArray_setitem(char **pstStringArray, size_t iIndex, char *pstValue) {
  pstStringArray[iIndex] = strdup(pstValue);
}
%}

char **new_StringArray(size_t iCount);
void delete_StringArray(char **pstStringArray, int iCount);
char *StringArray_getitem(char **pstStringArray, size_t iIndex);
void StringArray_setitem(char **pstStringArray, size_t iIndex, char *pstValue);

