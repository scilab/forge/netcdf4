%include <matrix.i>

%include scinetcdf_lowlevel_names.i
%include scinetcdf_lowlevel_int_arrays.i
%include scinetcdf_lowlevel_carrays.i
%include scinetcdf_lowlevel_sizet_arrays.i
%include scinetcdf_lowlevel_char.i

%apply (size_t *name_len, char *name) { (size_t *lenp, char *full_name) }

%apply (int *INOUT_SIZE, int* INOUT) { (int *nvars, int *varids) };
%apply (int *INOUT_SIZE, int* INOUT) { (int *ndims, int *dimids) };
%apply (int *INOUT_SIZE, int* INOUT) { (int *ntypes, int *typeids) };
%apply (int *INOUT_SIZE, int* INOUT) { (int *numgrps, int *ncids) };
%apply (int *INOUT_SIZE, int* INOUT) { (int *ndimsp, int *dimidsp) };

%apply (int IN_SIZE, int *IN) { (int ndims, const int *dimidsp) };

%apply int *OUTPUT { int *ncidp };
%apply int *OUTPUT { int *grp_ncid };
%apply int *OUTPUT { int *varidp };
%apply int *OUTPUT { int *idp };
%apply int *OUTPUT { int *new_ncid };
%apply int *OUTPUT { int *typeidp };
%apply int *OUTPUT { nc_type *OUTPUT };
%apply int *OUTPUT { size_t *lenp };
%apply int *OUTPUT { int *ndimsp };
%apply int *OUTPUT { int *nvarsp };
%apply int *OUTPUT { int *nattsp };
%apply int *OUTPUT { int *unlimdimidp };
%apply int *OUTPUT { int *parent_ncid };
%apply int *OUTPUT { int *formatp };

int nc_inq_grpname(int ncid, char *out_name);
int nc_inq_compound_name(int ncid, nc_type xtype, char *out_name);
int nc_inq_dimname(int ncid, int dimid, char *out_name);
int nc_inq_attname(int ncid, int varid, int attnum, char *out_name);
int nc_inq_varname(int ncid, int varid, char *out_name);
int nc_inq_dim(int ncid, int dimid, char *out_name, int *OUTPUT);
int nc_inq_var(int ncid, int varid, char *out_name, nc_type *OUTPUT,
    int *ndimsp, int *dimidsp, int *nattsp);
int nc_inq_vartype(int ncid, int varid, nc_type *OUTPUT);
int nc_inq_att(int ncid, int varid, char *name, nc_type *OUTPUT, size_t *lenp);
int nc_inq_atttype(int ncid, int varid, const char *name, nc_type *OUTPUT);

// nc_put_var1_type, nc_put_var1_type

%define %apply_numerictypes(Macro)
Macro(signed char, schar);
Macro(unsigned char, uchar);
Macro(short, short);
Macro(unsigned short, ushort);
Macro(int, int);
Macro(unsigned int, uint);
Macro(long, long);
Macro(float, float);
Macro(double, double);
%enddef

%define %nc_put_get_var1_type(Arg1, Arg2)
int nc_put_var1_ ## Arg2(int ncid, int varid, const size_t *INPUT, const Arg1 *INPUT);
int nc_get_var1_ ## Arg2(int ncid, int varid, const size_t *INPUT, Arg1 *OUTPUT);
%enddef

%apply_numerictypes(%nc_put_get_var1_type)

int nc_put_var1_text(int ncid, int varid, const size_t *INPUT, const char *INPUT);
int nc_get_var1_text(int ncid, int varid, const size_t *INPUT, char *OUTPUT);

int nc_put_var1_string(int ncid, int varid, const size_t *INPUT, const char **INPUT);
int nc_get_var1_string(int ncid, int varid, const size_t *INPUT, char **OUTPUT);

%ignore nc_put_var1_longlong;
%ignore nc_get_var1_longlong;
%ignore nc_put_var1_ulonglong;
%ignore nc_get_var1_ulonglong;

// nc_put_att_type, nc_get_att_type typemaps

%ignore nc_put_att_longlong;
%ignore nc_get_att_longlong;
%ignore nc_put_att_ulonglong;
%ignore nc_get_att_ulonglong;

// nc_put_vara_type, nc_get_vara_type typemaps

%apply size_t *INPUT { size_t *startp };
%apply size_t *INPUT { size_t *countp };
