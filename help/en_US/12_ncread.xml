<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2015 - Scilab Enterprises
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en"
  xml:id="ncread">
    <refnamediv>
        <refname>ncread</refname>
        <refpurpose>Reads the data from a variable in a NetCDF source.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>vardata = ncread(source, varname, [start, [count, [stride]]])</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>source</term>
                <listitem><para>Path to a NetCDF file or URL of an OpenNDAP source (string).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>varname</term>
                <listitem><para>Name of a existing variable (string).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>start</term>
                <listitem><para>Optional indexes at which the data is read (vector of double, each greater or eaqual to 1).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>count</term>
                <listitem><para>Optional number of values to read in each dimension (vector of double).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>stride</term>
                <listitem><para>Optional inter-element spacing (vector of double).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>vardata</term>
                <listitem><para>Data in the variable (scalar, matrix or hypermatrix of double/integers, or a string).</para></listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para><function>ncread</function> reads the data of a variable <term>varname</term> from a <term>source</term> (NetCDF file or OpenNDAP source).</para>

        <para>The location of the variable (i.e. the path of group at which the variable is stored) are specified in <term>varname</term> using the form <literal>/grp/subgrp/.../varname</literal>.</para>

        <para>The <term>vardata</term> Scilab type is the closest type of the variable NetCDF type.</para>

        <para>In the case of <literal>double/float</literal> variables, missing values or values equal to the fill value are automatically replaced by <literal>%nan</literal> during the reading. The fill value is either the value specified in a parameter of <link linkend="nccreate">nccreate</link> or, by default the value defined by NetCDF.</para>

        <para>The optional parameters <term>start</term>, <term>count</term>, <term>stride</term> are used to specify the indexes of data to read.</para>.
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example">
        <![CDATA[
        scinetcdf_path = getSciNetCDFPath();
        source = fullfile(scinetcdf_path, 'tests/examples/vars.nc');

        // Reads the double 'x'
        ncread(source, 'x')

        // Reads the variable 'y' in the group 'grp'
        ncread(source, 'grp/y')

        // Reads the variable 'n' (int8 scalar)
        ncread(source, 'n')

        // Reads the string 'str'
        ncread(source, 'str')

        // Reads the 1D double array 'arr1'
        ncread(source, 'arr1')

        // Reads the values of the 1D array 'arr1'
        ncread(source, 'arr1')

        // Reads the values of the 2D array 'arr2'
        ncread(source, 'arr2')
        ]]>
        </programlisting>
    </refsection>
    <refsection role="see also">
        <title>See Also</title>
        <simplelist type="inline">
            <member><link linkend="ncwrite">ncwrite</link></member>
            <member><link linkend="nccreate">nccreate</link></member>
        </simplelist>
    </refsection>
</refentry>
