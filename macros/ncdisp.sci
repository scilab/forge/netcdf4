function output = ncdisp(source, varargin)
    output = [];

    rhs = argn(2);
    if rhs < 1 then
        error("ncdisp: Wrong number of input argument(s): 1 to 3 expected.");
    end

    thirdparty_path = fullfile(getSciNetCDFPath(), "thirdparty");

    os = getos();
    [version, opts] = getversion();
    arch = opts(2);
    bin_path = fullfile(thirdparty_path, os, arch, "bin");
    if getos() == "Windows" then
        ncdump_path = fullfile(bin_path, "ncdump.exe");
        ncdump_cmd = ncdump_path;
    else
        lib_path = fullfile(thirdparty_path, os, arch, "lib");
        ncdump_path = msprintf("%s/ncdump", bin_path);
        ncdump_cmd = msprintf("LD_LIBRARY_PATH=%s %s", lib_path, ncdump_path);
    end

    full_disp = %t;
    console_disp = %t;
    location = [];

    if rhs > 1 then
        location = varargin(1);
        if rhs > 2 then
            disp_format = varargin(2);
            if find(disp_format == "min") <> [] then
                full_disp = %f;
            end
            if find(disp_format == "to_var") <> [] then
                console_disp = %f;
            end
        end
    end

    if ~full_disp then
        ncdump_cmd = ncdump_cmd + " -h";
    end

    if ~isempty(location) & (location <> '/') then
        if part(location, $) == '/' then
            location = part(location, 1:$-1);
            ncdump_cmd = ncdump_cmd + " -g " + location;
        else
            ncdump_cmd = ncdump_cmd + " -v " + location;
        end
    end

    output_filepath = tempname();
    err_filepath = tempname();
    ncdump_cmd = msprintf("%s ""%s"" >%s 2>%s", ncdump_cmd, source, output_filepath, err_filepath);

    [out, ret, err] = unix_g(ncdump_cmd);

    if ret == 0 then
        fd = mopen(output_filepath);
        while ~meof(fd) do
            txt = mgetl(fd, 1000);
            if ~console_disp then
                output = [output; txt];
            else
                disp(txt);
            end
        end
        mclose(fd);
        deletefile(err_filepath);
        deletefile(output_filepath);
    else
        deletefile(output_filepath);
        if isempty(err) then
            fd = mopen(err_filepath);
            err = mgetl(fd);
            mclose(fd);
        end
        deletefile(err_filepath);
        err = strsubst(err, ncdump_path + ': ', '');
        error(msprintf("ncdisp: %s.", err));
    end
endfunction
