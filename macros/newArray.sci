function array = newArray(typeName, arraySize)
    if argn(2) <> 2 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "newArray", 2));
    end
    if typeof(typeName) <> "string"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n"), "newArray", 1));
    end
    if typeof(arraySize) <> "constant"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A double expected.\n"), "newArray", 2));
    end

    arrayTypeName = getArrayTypeName(typeName);
    execstr(msprintf("array = new_%s(arraySize);", arrayTypeName));
endfunction

