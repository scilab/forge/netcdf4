function ncgen(in_filepath, out_filepath)
    rhs = argn(2);
    if rhs < 1 then
        error("ncgen: Wrong number of input argument(s): 2 expected.");
    end

    thirdparty_path = fullfile(getSciNetCDFPath(), "thirdparty");

    os = getos();
    [version, opts] = getversion();
    arch = opts(2);
    bin_path = fullfile(thirdparty_path, os, arch, "bin");
    if getos() == "Windows" then
        ncgen_cmd = fullfile(bin_path, "ncgen.exe");
    else
        lib_path = fullfile(thirdparty_path, os, arch, "lib");
        ncgen_cmd = msprintf("LD_LIBRARY_PATH=%s %s/ncgen", lib_path, bin_path);
    end

    ncgen_cmd = msprintf("%s -k nc4 -o %s %s", ncgen_cmd, out_filepath, in_filepath);
    [out, ret, err] = unix_g(ncgen_cmd);
    if (ret <> 0)
        error(err);
    end
endfunction
