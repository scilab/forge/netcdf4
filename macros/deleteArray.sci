function deleteArray(array, typeName, varargin)

    if argn(2) < 2 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "deleteArray", 2));
    end
    if (typeName == "string") & (argn(2) < 3)
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "deleteArray", 3));
    end
    if typeof(typeName) <> "string"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n"), "deleteArray", 3));
    end

    arrayTypeName = getArrayTypeName(typeName);
    if typeName <> "string" then
        execstr(msprintf("delete_%s(array);", arrayTypeName));
    else
        execstr(msprintf("delete_StringArray(array, %d);", varargin(1)));
    end
endfunction

