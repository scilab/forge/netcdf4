function arrayTypeName = getArrayTypeName(typeName)
    if argn(2) <> 1 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "getArrayTypeName", 1));
    end
    if typeof(typeName) <> "string"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n"), "getArrayTypeName", 1));
    end

    select typeName
        case "schar"
          arrayTypeName = "SCharArray";
        case "uchar"
          arrayTypeName = "UCharArray";
        case "short"
          arrayTypeName = "ShortArray";
        case "ushort"
          arrayTypeName = "UShortArray";
        case "uint"
          arrayTypeName = "UIntArray";
        case "int"
          arrayTypeName = "IntArray";
        case "long"
          arrayTypeName = "LongArray";
        case "float"
          arrayTypeName = "FloatArray";
        case "double"
          arrayTypeName = "DoubleArray";
        case "char"
          arrayTypeName = "CharArray";
        case "string"
          arrayTypeName = "StringArray";
        else
          error(msprintf("getArrayTypeName(): %s type is unknown.", typeName));
      end
endfunction
